<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Article;

class ArticleTransformer extends TransformerAbstract
{
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
        //
    ];
    
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        //
    ];
    
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Article $article)
    {
        return [
            //
            'id' => $article->id,
            'title' => $article->title,
            'content' => $article->content,
            'writer_name' => $article->writer_name,
            'cover' => url('/') . '/storage/' . $article->cover,
            'status' => $article->status,
            'created_at' => $article->created_at->toDateString(),
            'updated_at' => $article->updated_at
        ];
    }
}
