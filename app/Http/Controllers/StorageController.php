<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;

class StorageController extends Controller
{
    //
    public function fetchFile(Request $request, $file_name)
    {
        //fetch user from token
//        $user = FetchInfo::getUserObject($request);
//return public_path();
        $path = '/var/projects/lims-backend/public/' . $file_name;
        if (!File::exists($path)) {
                return 'image not found';
        }
        $file = File::get($path);
        $type = File::mimeType($path);
        $response = Response::make($file, 200);
        $response->header("Content-Type", $type);
        return $response;
    }
}
