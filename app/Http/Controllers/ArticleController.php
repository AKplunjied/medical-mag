<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Article;
use App\Category;
use Spatie\Image\Image as Simage;
use Intervention\Image\ImageManagerStatic as Image;
use League\Fractal\Serializer\ArraySerializer;
use App\Transformers\ArticleTransformer;

class ArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $articles = Article::query()->where('status','active')->get();
        return fractal()
            ->serializeWith(new ArraySerializer())
            ->collection($articles)
            ->transformWith(new ArticleTransformer())
            ->respond();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getMainFeaturedArticle()
    {
        //
        $articles = Article::query()->where('status','active')->where('featured_flag',1)->first();
        return fractal()
            ->serializeWith(new ArraySerializer())
            ->item($articles)
            ->transformWith(new ArticleTransformer())
            ->respond();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {//return $request;

        if(isset($request->cover_photo))
        {
            $request['cover'] = self::storeImage($request->cover_photo);
        }else{
            $request['cover'] = 'default.png';
        }
        $article = Article::query()->create($request->all());
        $category = Category::query()->where('name',$request->category_id)->value('id');
        $article->categories()->attach($category);
        
        return response()->json(['error' => false,
        'message' => 'Article added']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $article_id)
    {
        //
        $article = Article::query()->find($article_id);
        return fractal()
            ->serializeWith(new ArraySerializer())
            ->item($article)
            ->transformWith(new ArticleTransformer())
            ->respond();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    private static function storeImage($file)
    {
        //RENAME THE UPLOADED IMAGE
        $photoName = rand(0, 4) . time() . '.' . $file->getClientOriginalExtension();
        //RESIZE IT
        $image_resize = Image::make($file);
        $width = 896; // your max width
        $height = 600; // your max height
        $image_resize->height() > $image_resize->width() ? $width=null : $height=null;
        $image_resize->resize($width, $height, function ($constraint) {
            $constraint->aspectRatio();
        });
        $image_resize->save(public_path('/') . $photoName);

        //MAKE A THUMBNAIL COPY
        $thumbname = 't' . $photoName;
        Simage::load(public_path('/') . $photoName)
            ->width(200)
            ->height(200)
            ->save(public_path('/') . $thumbname);
        return $photoName;
    }

    public function search(Request $request)
    {
        $articles = Article::query()->where('title','like','%'.$request->search.'$')
        ->orwhere('content','like','%'.$request->search.'%')->get();
        return fractal()
            ->serializeWith(new ArraySerializer())
            ->collection($articles)
            ->transformWith(new ArticleTransformer())
            ->respond();
    }
}
