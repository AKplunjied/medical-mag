<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use League\Fractal\Serializer\ArraySerializer;
use App\Http\Requests\CategoryRequest;
use App\Transformers\CategoryTransformer;
use App\Category;
use App\Transformers\ArticleTransformer;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $categories = Category::query()->where('status','active')->get();
        return fractal()
            ->serializeWith(new ArraySerializer())
            ->collection($categories)
            ->transformWith(new CategoryTransformer())
            ->respond();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CategoryRequest $request)
    {
        //
        Category::query()->create($request->all());
        return response()->json(['error' => false, 'message' => 'Category Added']); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $category_id)
    {
        //
        $category = Category::query()->find($category_id);
        $articles = $category->articles()->where('status','active')->get();
        return fractal()
            ->serializeWith(new ArraySerializer())
            ->collection($articles)
            ->transformWith(new ArticleTransformer())
            ->respond();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getCategoryArticles(Request $request, $category_id)
    {
        
    }
}
