<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\URL;

/**
 * Class NoCache
 * @package App\Http\Middleware
 */
class customHeaders
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = $next($request);

        $response->headers->set('Cache-Control', 'no-cache, must-revalidate');
        $response->headers->set('Strict-Transport-Security', 'max-age=86400 , includeSubDomains');
        $response->headers->set('Access-Control-Allow-Origin', '*');
        $response->headers->set('Access-Control-Expose-Headers', 'Content-Length, X-JSON, content-type');
        $response->headers->set('Access-Control-Allow-Methods', 'GET, POST, PATCH, PUT, DELETE, OPTIONS');
        $response->headers->set('Access-Control-Allow-Headers', '*');
        $response->headers->set('X-Frame-Options', 'deny');
        $response->headers->set('X-XSS-Protection', '1; mode=block');
        $response->headers->set('X-Content-Type-Options', 'nosniff');
        $response->headers->set('Referrer-Policy', 'no-referrer-when-downgrade');
        $response->headers->set('Feature-Policy', '');
        return $response;
    }
}
