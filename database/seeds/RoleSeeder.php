<?php

use Illuminate\Database\Seeder;
use App\Role;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $roles = ['admin', 'moderator', 'user'];
        foreach($roles as $role)
        {
            Role::firstOrCreate([
                'name' => $role,
                'status' => 'active'
            ]);
        }
    }
}
