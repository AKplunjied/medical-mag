<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRelations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
                $table->foreign('gender_id')->references('id')->on('gender');
        });
        Schema::table('user_role', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('role_id')->references('id')->on('role');
        });
        Schema::table('article', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users');
        });
        Schema::table('article_category', function (Blueprint $table) {
            $table->foreign('article_id')->references('id')->on('article');
            $table->foreign('category_id')->references('id')->on('category');
        });
        Schema::table('article_tag', function (Blueprint $table) {
            $table->foreign('article_id')->references('id')->on('article');
            $table->foreign('tag_id')->references('id')->on('tag');
        });
        Schema::table('comment', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('article_id')->references('id')->on('article');
        });
        Schema::table('article_media', function (Blueprint $table) {
            $table->foreign('article_id')->references('id')->on('article');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('relations');
    }
}
